import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(value: string): string {
    const words = value.split(' ');
    const capitalizeWords = words.map(
      (word) => word[0].toUpperCase() + word.slice(1)
    );
    return capitalizeWords.join(' ');
  }
}
