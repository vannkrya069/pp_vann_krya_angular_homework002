import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ParentComponent } from './parent/parent.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CapitalizePipe } from './capitalize.pipe';
import { SortAlphabetPipe } from './sort-alphabet.pipe';
import { BooleanDirectiveDirective } from './boolean-directive.directive';
import { RightContentComponent } from './right-content/right-content.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    SidebarComponent,
    RightContentComponent,
    CapitalizePipe,
    SortAlphabetPipe,
    BooleanDirectiveDirective,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
