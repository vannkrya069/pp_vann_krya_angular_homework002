import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-right-content',
  templateUrl: './right-content.component.html',
})
export class RightContentComponent {

  contents: string[] = [
    'Dynamic reports and dashboards',
    'Templates for everyone',
    'Development workflow',
    'Limitless business automation',
  ];

  checkBoxImage: string = '../../assets/images/checkbox.png';
  angularImage: string = '../../assets/images/angular.png';

  @Input() subjectListsFromParent!: any[];
  
}
