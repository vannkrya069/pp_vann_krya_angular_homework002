import { Component } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
})
export class ParentComponent {
  bool: boolean = true;
  recieveSubjectLists: any[] = [
    {
      subjectName: 'Angular',
      description: `Angular is an open framework and
  platform for creating Single Page Applications, written in TypeScript and supported and developed by Google.`,
      thumbnail: '../../assets/images/angular.svg',
    },
  ];

  login(e: boolean) {
    this.bool = !this.bool;
  }

  logout(event: boolean) {
    this.bool = event;
  }

  recieveSubjectList(e: any[]) {
    console.log(e);
    this.recieveSubjectLists = e;
  }
}
