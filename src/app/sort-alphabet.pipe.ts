import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortAlphabet'
})
export class SortAlphabetPipe implements PipeTransform {

  transform(value: any[], property: string): any[] {
    if (!Array.isArray(value)) {
      return value;
    }
    return value.slice().sort((a, b) => a[property].localeCompare(b[property]));
  }

}
